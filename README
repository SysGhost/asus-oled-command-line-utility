###############################################################################
#### About this project #######################################################
###############################################################################
Some Asus laptops comes with neat little status displays, but few softwares
are around that can utilize them. Specially when it comes to the 
Linux Operating System.

What I've made here, is a simple and easy to use command line utilities that 
communicates with the display.
With this utility, and a bunch of simple pixmap images, anyone can easily 
create messages, animations and transitions without the need of deep
knowledge of the underlying system.

See http://www.youtube.com/watch?v=2i8Twa4h_iI for a short video-demostration.
In the demostration above I only executed a few short commands. 
The utilities took care of the rest and threw around a bunch of pixmap 
images for me.

This makes it perfect for boot animations, event updates, status changes 
and alike.



sysghost@gmail.com


###############################################################################
#### Known bugs ###############################################################
###############################################################################
  * Cannot use files with spaces in their names due to a limitation in 'getopt'
  * Lower-than-usual framerate on animations at some times due to perfomance limitations in shell scripts.

###############################################################################
#### To fix ###################################################################
###############################################################################
  * Mentioned bugs. Of course.
  * Grammar in the help sections. English isn't my strongest side.
  * Rewrite utilities in a better language. Preferebly a script language so non tech-savvy users can edit them easily. (Python?)

###############################################################################
#### asus_oled-ctl ############################################################
###############################################################################
  "asus_oled-ctl" is a simple control utility.
  It can control Asus G1/G2/G50/G70/G71 OLED displays.
  This utility depends on 'xpm2asus_oled', which should have been provided.
 
  Avaliable options are:  
     -p, --power <STATE>  
          Turns oled display on or off.
          Arguments: 
               <STATE> (string)
                    on    - enables the oled display.
                    off   - disables the oled display.

     -s, --static-picture <FILE>  
          Display <FILE> on oled display.
          If --cycle is also present, it will execute first.
          Once cycle has finnished, the given <FILE> will be displayed.
          (Unless cycle is endless).
                                          
     -c, --cycle <DELAY>,<CYCLES>[,<CACHE-ID>] 
          Will cycle through the given list of pictures or cache.
          Useful for animations.
          Arguments: 
               <DELAY> (int)
                    Milliseconds to delay each picture in file list.
               <CYCLES> (int)
                    Number of times to cycle.
                    If set to 0, it will cycle indefinitely, until interupted.
               <CACHE-ID> (string)
                    Use a pregenerated cache instead of the given file list.
                    This skips the conversion process, which will 
                    speed up things considerably.
                    See '--generate-cache' option for more details.

     -r, --reverse
          Used together with '--cycle' or '--generate-cache'.
          Generates a reversed cycle.

     -g, --generate-cache <CACHE-ID>
          Converts a given list of pictures and store them in cache-slot 
          with ID <CACHE-ID>.
          Arguments:
               <CACHE-ID>
                    An identifier for the cache slot. 
                    E.g: --generate-cache 'Boot_Animation'

     -o, --show-cache
          List available cache-slot IDs. 

     -d, --delete-cache <CACHE-ID>
          Remove cache-slot with ID <CACHE-ID>. 
          Arguments:
               <CACHE-ID>
                    An identifier for the cache slot. 
                    E.g: --delete-cache 'Boot_Animation'

     -l, --clear 
          Clears everything on the display.

     -f, --force-run
          Skips /sys/class checks and ignores kernel module status.
     
     -h, --help
          Show this text.


  Examples:
     asus_oled-ctl --power on
     asus_oled-ctl --power off
          Turns oled display on or off.

     asus_oled-ctl --static-picture image_128x32_mono.xpm 
          Displays 'image_128x32_mono.xpm' on the oled display.

     asus_oled-ctl --cycle 200,8 image*.xpm
     asus_oled-ctl --cycle 200,8 image[0-9][0-9].xpm
     asus_oled-ctl --cycle 200,8 image01.xpm image02.xpm image03.xpm 
          Cycle through image01.xpm, image02.xpm ... image{nn}.xpm 8 times 
          with 200 milliseconds pause between each picture.

     asus_oled-ctl --static-picture logo.xpm --cycle 500,2 image*.xpm 
          Cycle through image01.xpm, image02.xpm ... image{nn}.xpm 2 times 
          with 500 milliseconds pause between each picture.
          Once done, it will display logo.xpm and exit.

     asus_oled-ctl --generate-cache 'Boot-Animation' image*.xpm 
          Converts all files matching 'image*.xpm'
          Stores the generated result in a slot named 'Boot-Animation'
     
     asus_oled-ctl --cycle 50,0,'Boot-Animation'
          Cycle through a cache-slot named 'Boot-Animation' indefinitely (0)
          with 50 milliseconds pause between each precached frame. 
          (50 ms delay = 20 frames per second)



###############################################################################
#### xpm2asus_oled ############################################################
###############################################################################
	"xpm2asus_oled" is a simple conversion utility.
	Converts X11 Pixmap Graphic data (XPM) that can be used on some 
  Asus laptops OLED displays.
	Output can be written directly to /sys/class/asus_oled/oled_1/picture
	
	Prefered xpm format:
	1 bit monocrome with white foreground is prefered for the asus oled display. 
	Other colours are ignored by default.
	Multible XPM palettes are possible, where the display output can be 
	optimized with an option: '--palette'.


  Available options are:  
	-l, --list-colours
		List colours in the xpm palette.

	-p, --palette <CSV-STRING>
		Use custom palettes for lit pixels on the oled display. 
		Given colours in the palette must match one or more colours 
		in the files palette. See '--list-colours' option.
		Where elements of <CSV-STRING> can be: (including apostrophes)
			'Named colour'   - A named colour. e.g. 'Green'
			'#xxxxxx'        - RGB Hex value. e.g. '#00FF00'
      'Symbolic'       - A symbolic name. e.g. 'lit_pixel'
			'None'           - Exact as is. Used for transparent pixels.
		It is also possible to specify a palette.
		Just add a colon ':' followed by the palette column (m, g4, g, c and s).
		Example:
			'#FFFFFF:m'     - Selects hex value '#FFFFFF' from palette 'm'.
			'Red:c'         - Selects named value 'Red' from palette 'c'.
		See preseding symbols (m,g4,g,c,s) '--list-colours' header.

	-m, --mode <DISPLAY-MODE>
		Changes display mode. ( * marks default. )
		NOTE: Effect is only visible on oled displays.
		Where <DISPLAY-MODE> can be a single character of choice:
			s        * (S)tatic. Simply diplays an image.
			f        - (F)lashing. Flashes the image.
			r        - (R)olling. Rolls the image from right to left.

	-o, --output <OUTPUT-MODE>
		Changes the output mode. ( * marks default. )
		NOTE: ANSI corrupts output on oled displays. Useful for console only.
			a        - ' ' and '#' for pixels. Can be read by oled displays.
			b        * '0' and '1' for pixels. Can be read by oled displays.
			ansi     - Ansi escape sequences. Cannot be read by displays.
			
			           By appending a colon followed by two characters, 
			           these two characters will replace the default blank 
			           spaces.
			           
			           Example: '-o ansi:XY'. 
			             First character 'X' replaces unlit areas, 
			             and second character 'Y' will replace lit areas.
			             
			           Ansi escape qequences can be customised with the 
			           optional '--ansi' option.

	-s, --ansi <'SEQ0','SEQ1'>
		Activates ansi sequences of choice on output.
		Make sure to enclose the sequences with single quotes. Otherwise the 
		command interpreter will catch the sequences before they're 
		fed into this script.
		NOTE: ANSI corrupts output on oled displays. Useful for console only.
			SEQ0     - ANSI escape sequence for unlit pixels.
			SEQ1     - ANSI escape sequence for lit pixels.
			
		Example: 
			--ansi '\x5C033[32;42m','\x5C033[31;41m'

	-h, --help
		Show this page.

  Examples: 
    xpm2asus_oled image.xpm
      Convert white (#FFFFFF) pixels in image.xpm to monocrome.
      Write the converted data to standard output.
      All the other colours are ignored.
    
    xpm2asus_oled --palette '#FF0000' image.xpm
      Convert #FF0000 (Red) pixels in image.xpm to monocrome.
      RGB hex only works on pictures where the palette have colours defined 
      in RGB hex. 
      Check with '--list-colurs' option.
    
    xpm2asus_oled --palette 'Red','Green','White:c' image.xpm
      Convert Red, Green and White:c pixels in image.xpm to monocrome. 
      Uses Red and Green from any palette, and White from palette 'c'.
      Palette selection (colour:palette) can prove useful if more than one 
      palette have the same colour defined, but for different pixels.
      Named colours only works on pictures where the palette have colours
      defined with names.
      Check this with '--list-colurs' option.
    
    xpm2asus_oled --mode r image.xpm
      Converts image.xpm, and activates the (r)olling effect once 
      output has been fed to the Asus oled display.
      NOTE: Effects are only visible on oled displays.













###############################################################################
### Additional details about tools and formats I've used to create this. ######
###############################################################################
  About 'X11 Pixmap Format':
    Additional information: 
    Copyright (C) 1989-95 GROUPE BULL
    License: 'As-is'
       Permission is hereby granted, free of charge, to any person obtaining a 
       copy of this software and associated documentation 
       files (the "Software"), to deal in the Software without restriction, 
       including without limitation the rights to use, copy, modify, merge, 
       publish, distribute, sublicense, and/or sell copies of the Software,
       and to permit persons to whom the Software is furnished to do so, 
       subject to the following conditions:
       The above copyright notice and this permission notice shall be 
       included in all copies or substantial portions of the Software.
       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
       EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
       MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
       IN NO EVENT SHALL GROUPE BULL BE LIABLE FOR ANY CLAIM, DAMAGES OR 
       OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
       ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
       OTHER DEALINGS IN THE SOFTWARE.
       Except as contained in this notice, the name of GROUPE BULL shall not 
       be used in advertising or otherwise to promote the sale, use or other 
       dealings in this Software without prior written authorization from 
       GROUPE BULL.

    Additional information: 
      http://www.xfree86.org/current/xpm.pdf



  About 'GIMP':
    License: 'GNU Lesser General Public License'
    Copyright © 2001-2012 The GIMP Team 
      GIMP is Free Software and a part of the GNU Project. 

    Additional information: 
      http://www.gimp.org/docs/



  About 'bash':
    License: 'GNU General Public License'
    Copyright © 2007, 2009, 2011 Free Software Foundation, Inc.
      Bash is free software; you can redistribute it and/or modify it under 
      the terms of the GNU General Public License as published by the 
      Free Software Foundation; either version 3 of the License, 
      or (at your option) any later version.

    Additional information: 
      http://www.gnu.org/software/bash/bash.html



