#!/bin/bash
################################################################################
# 'xpm2asus_oled' is a simple conversion utility.
# It converts XPM image data for the Asus G1/G2/G50/G70/G71 oled displays.
# Output can be written directly to /sys/class/asus_oled/oled_1/picture
# 
# 
# 
# 
# 
# Commands needed: 
#   coreutils: cat, cut, od, head, echo, printf, basename
#   ohters: grep, sed, perl
#
# sysghost@gmail.com
################################################################################
declare CWP="`dirname $(realpath $0)`" # Do not change. Script relies on this.
source "${CWP}/../conf/asus_oled.conf"

declare version="0.1.2"
declare default_colour="$DEFAULT_COLOUR"
declare output="$DEFAULT_OUTPUT_MODE"
declare mode="$DEFAULT_DISPLAY_MODE"
declare file
declare xpm_image_data
declare width
declare height
declare n_colours
declare chars_per_pixel
declare -a colour_chars
declare -a users_colour_list
declare -a output_user
declare output_ansi_seq[0]="$DEFAULT_OUT_ANSI_0"
declare output_ansi_seq[1]="$DEFAULT_OUT_ANSI_1"
declare output_ansi_char[0]="$DEFAULT_OUT_CHAR_0"
declare output_ansi_char[1]="$DEFAULT_OUT_CHAR_0"

declare progname="`basename $0`"
declare -a help_text[0]="\
Usage:  ${progname} [--palette <colours>] [--output <a|b|ansi>] [--mode <s|f|r>] [--ansi <seq,seq>] FILE
        ${progname} --list-colours FILE
"
declare -a help_text[1]="\
Try '${progname} --help' for more information.
"
declare help_text_long="\
${help_text[0]}

	'$progname' is a simple conversion utility.
	Converts X11 Pixmap Graphic data (XPM) that can be used for some Asus laptops oled display.
	Output can be written directly to /sys/class/asus_oled/oled_1/picture
	
	Prefered xpm format:
	1 bit monocrome with white foreground is prefered for the asus oled display. 
	Other colours are ignored by default.
	Multible XPM palettes are possible, where the display output can be 
	optimized with an option: '--palette'.


Options are:  
	-l, --list-colours
		List colours in the xpm palette.

	-p, --palette <CSV-STRING>
		Use custom palettes for lit pixels on the oled display. 
		Given colours in the palette must match one or more colours 
		in the files palette. See '--list-colours' option.
		Where elements of <CSV-STRING> can be: (including apostrophes)
			'Named colour'   - A named colour. e.g. 'Green'
			'#xxxxxx'        - RGB Hex value. e.g. '#00FF00'
			'Symbolic'       - A symbolic name. e.g. 'lit_pixel'
			'None'           - Exact as is. Used for transparent pixels.
		It is also possible to specify a palette.
		Just add a colon ':' followed by the palette column (m, g4, g, c and s).
		Example:
			'#FFFFFF:m'     - Selects hex value '#FFFFFF' from palette 'm'.
			'Red:c'         - Selects named value 'Red' from palette 'c'.
		See preseding symbols (m,g4,g,c,s) '--list-colours' header.

	-m, --mode <DISPLAY-MODE>
		Changes display mode. ( * marks default. )
		NOTE: Effect is only visible on oled displays.
		Where <DISPLAY-MODE> can be a single character of choice:
			s        * (S)tatic. Simply diplays an image.
			f        - (F)lashing. Flashes the image.
			r        - (R)olling. Rolls the image from right to left.

	-o, --output <OUTPUT-MODE>
		Changes the output mode. ( * marks default. )
		NOTE: ANSI corrupts output on oled displays. Useful for console only.
			a        - ' ' and '#' for pixels. Can be read by oled displays.
			b        * '0' and '1' for pixels. Can be read by oled displays.
			ansi     - Ansi escape sequences. Cannot be read by displays.
			
			           By appending a colon followed by two characters, 
			           these two characters will replace the default blank 
			           spaces.
			           
			           Example: '-o ansi:XY'. 
			             First character 'X' replaces unlit areas, 
			             and second character 'Y' will replace lit areas.
			             
			           Ansi escape qequences can be customised with the 
			           optional '--ansi' option.

	-s, --ansi <'SEQ0','SEQ1'>
		Activates ansi sequences of choice on output.
		Make sure to enclose the sequences with single quotes. Otherwise the 
		command interpreter will catch the sequences before they're 
		fed into this script.
		NOTE: ANSI corrupts output on oled displays. Useful for console only.
			SEQ0     - ANSI escape sequence for unlit pixels.
			SEQ1     - ANSI escape sequence for lit pixels.
			
		Example: 
			--ansi '\x5C033[32;42m','\x5C033[31;41m'

	-v, --version
		Show version.

	-h, --help
		Show this page.

Examples: 
	${progname} image.xpm
		Convert white (#FFFFFF) pixels in image.xpm to monocrome.
		Write the converted data to standard output.
		All the other colours are ignored.

	${progname} --palette '#FF0000' image.xpm
		Convert #FF0000 (Red) pixels in image.xpm to monocrome.
		RGB hex only works on pictures where the palette have colours defined 
		in RGB hex. 
		Check with '--list-colurs' option.

	${progname} --palette 'Red','Green','White:c' image.xpm
		Convert Red, Green and White:c pixels in image.xpm to monocrome. 
		Uses Red and Green from any palette, and White from palette 'c'.
		Palette selection (colour:palette) can prove useful if more than one 
		palette have the same colour defined, but for different pixels.
		Named colours only works on pictures where the palette have colours
		defined with names.
		Check this with '--list-colurs' option.

	${progname} --mode r image.xpm
		Converts image.xpm, and activates the (r)olling effect once 
		output has been fed to the Asus oled display.
		NOTE: Effects are only visible on oled displays.
"


function main {
  init "$@"
  check_file          # ... Duh!
  load_n_scrub        # Load the dirty little thing and scrub it with a sed-soap. It'll hopefully end up with a less dirty, but useful blob of data.
  [[ $clist == "true" ]] && list_colours
  get_colour_chars    # Fill the colour_chars array with chosen colour data. These pixel colours will lit up.
  
  if [[ ${output:0:4} == "ansi" ]]; then
    if [[ ${output:4:1} == ":" ]]; then 
      output_ansi_char[0]="${output:5:1}"
      output_ansi_char[1]="${output:6:1}"
    fi
    convert_data        # Convert XPM data to a useful format for the oled display. Store cleaned data in 'converted_data'
    echo -e "$converted_data" | sed "s/\(X\{1,\}\)/$(printf ${output_ansi_seq[0]})\1$(printf "\033[0m")/g;s/\(Y\{1,\}\)/$(printf ${output_ansi_seq[1]})\1$(printf "\033[0m")/g;s/[X]/${output_ansi_char[0]}/g;s/[Y]/${output_ansi_char[1]}/g"
  else
    convert_data        # Convert XPM data to a useful format for the oled display. Store cleaned data in 'converted_data'
    echo -e "$converted_data"
  fi
}



function init {
  set -- `getopt -s bash -u -n$progname --longoptions="list-colours palette: output: mode: ansi: version help" "lp:o:m:s:vh" "$@" ` || usage "short"
  [ $# -lt 2 ] && usage "short"

  while [ $# -gt 0 ]
  do
      case "$1" in
        --list-colours|-l) clist="true";shift;;
        --palette|-c)      update_users_colour_list "$2";shift;;
        --output|-o)       output="$(echo -e $2)";shift;;
        --mode|-m)         mode=$2;shift;;
        --ansi|-s)         output_ansi_seq[0]="$(echo $2 | cut -d',' -f1)"; output_ansi_seq[1]="$(echo $2 | cut -d',' -f2)";shift;;
        --version|-v)      usage "version";;
        --help|-h)         usage "long";;
        --)                shift;break;;
        -*)                report_invalid_arg "$2";shift;;
        *)                 break;;
      esac
      shift
  done
  file="$@"
  
  if [[ $IAE ]]; then
    echo -e "${progname}: invalid option(s): ${IAL[@]}" >&2
    echo "Try '${progname} --help' for more information." >&2
    exit 64
  fi
}



function check_file {
  OLDIFS=$IFS
  IFS=$(echo -en "\n\b")
  if [ -f "$file" ]; then
    if [[ ! "`head -n1 "$file"`" == "/* XPM */" ]]; then
      echo -e "XPM header not found. Aborting.\nSeems it's not a xpm image." >&2
      exit 65
    fi
  else
    echo "${progname}: _File not found: '$file'" >&2
    exit 66
  fi
  IFS=$OLDIFS
}



function load_n_scrub {
  # Load the file with sed to filter out comments, C-specific stuff and to trim away spaces and tabs. 
  # Hopefully it spits out some clean and useful data. While it looks like overdoing it, it is sometimes needed. 
  # There are some dirty XPM editors floating around. Got a pretty big sed-soap down there just in case ;)
  while IFS= read -r line
  do
    xpm_image_data=("${xpm_image_data[@]}" "$line")
  done < <( sed 's/\/\*.*\*\///g;s/static char \*.*\[\]\s*\=\s*{//g;s/}\;//g;/^$/d;s/^\s*//g;s/\s*$//g;s/^"//g;s/",//g;s/"$//g' "$file" ) # The almighty sed-soap 
  
  # Put needed information into neat little variables. Makes things so much prettier. <3 (And much easier to read.)
  t_array=(${xpm_image_data[0]})
  width=${t_array[0]}
  height=${t_array[1]}
  n_colours=${t_array[2]}
  chars_per_pixel=${t_array[3]}
  unset t_array
}



function get_colour_chars {
  [ ${#users_colour_list[@]} -lt 1 ] && users_colour_list[0]="${default_colour}"
  for (( line=1; line<=$n_colours; line++ )); do
    for (( i=0; i<${#users_colour_list[@]}; i++ )); do
      if [[ "${users_colour_list[i]}" =~ ":" ]]; then
        colour_column="$(echo "${users_colour_list[i]}" | cut -d':' -f2)"
      else
        colour_column="."
      fi
      
      if [[ ${xpm_image_data[line]} =~ $( echo "$colour_column{1,2}\s{1,}$(echo ${users_colour_list[i]} | cut -d':' -f1)" ) ]]; then
        colour_chars=("${colour_chars[@]}" "$( echo -n "${xpm_image_data[line]:0:chars_per_pixel}" | od -A n -t x1 |sed 's/ /\\x/g' )")
      fi
    done
  done
}



function convert_data {
  for (( i=0; i<$height; i++ )); do 
    picture_data="${picture_data}${xpm_image_data[ i + n_colours + 1 ]}\n"
  done
  subst_str=$( head -c $chars_per_pixel /dev/zero | sed 's/\x00/\t/g' )
  for colour_char in ${colour_chars[@]}; do
    picture_data="$(echo "${picture_data}" | perl -pi -e "s/$colour_char/$subst_str/g" )"
  done
  case $output in
    "a")
      o[0]=' '
      o[1]='#'
    ;;
    "b")
      o[0]='0'
      o[1]='1'
    ;;
    "ansi"*)             # Ansi output mode. 'X' and 'Y' are just placeholders here. They'll be replaced later on.
      o[0]='X'
      o[1]='Y'
    ;;
    *)
      echo -e "${progname}: Invalid output option given. Using default." >&2
      o[0]='0'
      o[1]='1'
    ;;
  esac
  
  if [[ ! "${mode:0:1}" == [sfr] ]]; then 
    echo "$progname: Invalid mode argument given. Using static 's'." >&2
    mode="s"
  fi
  
  header="<${mode:0:1}:${width}x${height}>\n"
  data="$(echo -e "${picture_data}" | sed -r "s/[^\t]/${o[0]}/g;s/\t/${o[1]}/g;s/(.){$chars_per_pixel}/\1/g")"
  converted_data="${header}${data}"
}



function list_colours {
  # Extract colour palette from that ugly blob of data. Then hack'n'slash it into submission with a few nested for-loops. 
  # While at it, crub it with a sed-soap. These XPM palettes can be pretty darn dirty in some cases. Better safe than sorry.
  # While it looks like this function is overdoing it, it's needed. Some XPM files can be a real mess that makes eyes bleed.
  out="m) Monochrome:g4) 2bit Greys:g) 8bit Greys:c) Colour:s) Symbolic\n"
  out=${out}"--------------:--------------:--------------:--------------:--------------\n"
  for (( line=1; line<=$n_colours; line++ )); do
    row_template="{m}:{g4}:{g}:{c}:{s}"

    t_array=($( echo "${xpm_image_data[line]:chars_per_pixel + 1}" | sed 's/^\s*//g;s/\s*$//g;s/\s\{1,\}/ /g' ))
    for (( i=1, colour_column=0; colour_column<${#t_array[@]}; i++, colour_column=colour_column+2 )); do
      row_template="$( echo "$row_template" | sed "s/{${t_array[colour_column]}}/${t_array[colour_column + 1]}/g" )"
    done
    out=${out}"$( echo "$row_template" | sed "s/{[mgcs4]\{1,2\}}/ /g" )\n"
  done
  unset t_array
  echo -e $out | column -t -s':'
  exit 0
}



function update_users_colour_list {
  csv=$(echo $1 | tr "," "\n")
  for v in $csv
  do
    users_colour_list=("${users_colour_list[@]}" "$v")
  done
}



function update_mode_string {
  csv=$(echo $1 | tr "," "\n")
  for v in $csv
  do
    mode_string=("${mode_string[@]}" "$v")
  done
}



function usage {
  case $1 in
    "long")
    printf "$help_text_long" 
    ;;
    "short")
    printf "${help_text[0]}${help_text[1]}"
    ;;
    "version")
    printf "%s version: %s\n" $progname $version
    ;;
  esac
  exit 0
}


main "$@"
exit 0
